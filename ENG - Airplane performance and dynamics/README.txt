%%%%%%%%%%%%%%
%% WARNING! %%
%%%%%%%%%%%%%%

Previous years exams were structured differently since the course used to be taught differently. Mind you (!) that some questions may be about topics which are not taught by Riboldi anymore, even though the vast majority of the questions are the same.

D